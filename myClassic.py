import scipy.stats as st
from scipy.special import logsumexp
import math
class ASR:
    def __init__(self, phonemes_dictionary,  m0, S0, states=10):
        '''
        Initializes HMM
        self.A — statesxstates, array of transition probabilities, initialize randomly
        self.m — statesx39, mean of the p(o|i), initialize with global mean
        self.S — statesx39, variance of the p(o|i), initialize with global variance
        
        '''
        # TODO
        np.random.seed(777)
        self.A = np.random.sample((states, states))
        #print(self.A)
        for i in range(states):
            self.A[i,:]=self.A[i,:]/self.A[i,:].sum()
        self.A = np.log(self.A)
        #print(self.A)
        
        self.N = states
        self.m = np.zeros((self.N, 39))
        self.S = np.zeros((self.N, 39))
        for i in range(self.N):
            self.m[i,:]=m0
            self.S[i,:]=S0
        self.PI = [np.log(1.0/self.N)]*self.N
            
    def get_B(self, o, i):
        return st.multivariate_normal.pdf(o, mean=self.m[i,:].T, cov=np.diag(self.S[i, :]))

    def get_log_B(self, o, i):
        accum = 0
        for d in range(39):
            accum+=(np.log(2*math.pi)+self.S[i,d]+(o[d]-self.m[i,d])**2/self.S[i,d])
        return -0.5*accum
        
        
    def get_Alphas(self, obs):
        T = obs.shape[0]
        alphas = np.zeros((T, self.N))
        for t in range(T):
            alpha_i_sum = 0
            for i in range(self.N):
                #print(self.get_B(obs[0], i))
                if(t == 0):
                    #alphas[t, i] = self.PI[i]*self.get_B(obs[0], i)
                    alphas[t, i] = self.PI[i]+self.get_log_B(obs[0], i)
                else:
                    accum = np.zeros(self.N)
                    for j in range(self.N):
                        #alphas[t,i] += alphas[t-1, j]*self.A[j, i]
                        accum[j]=alphas[t-1, j]+self.A[j,i]
                    #print("Alpha:" + str(alphas[t,i]))
                    #print("B:" + str(self.get_B(obs[t], i)))
                    #alphas[t,i] *= self.get_B(obs[t], i)
                    #alphas[t,i] = np.log(alphas[t,i])+self.get_log_B(obs[t], i)
                    alphas[t,i] = logsumexp(accum) + self.get_log_B(obs[t], i)
            #alphas[t,:] /= alphas[t,:].sum()
            #print(alphas[t,:])
            
        return alphas
    
    def get_Betas(self, obs):
        betas = np.zeros((obs.shape[0], self.N))
        T = obs.shape[0]
        for t in reversed(range(0, T)):
            if(t == T-1):
                for i in range(self.N):
                    #betas[t,i] = 1
                    betas[t,i] = 0
            else:
                for i in range(self.N):
                    accum = np.zeros(self.N)
                    for j in range(self.N):
                        #betas[t,i] += self.A[i,j]*self.get_B(obs[t+1], j)*betas[t+1, j]
                        accum[j] = self.A[i,j]+self.get_log_B(obs[t+1],j)+betas[t+1, j]
                    betas[t, i] = logsumexp(accum)
        return betas
            
    #def get_P(self, obs):
    #        alpha_T = np.zeros(self.N)
    #        T = obs.shape[0]-1
    #        alphas = self.get_Alphas(obs)
    #        return alphas[T,:].sum()
    
    def get_P(self, obs):
        return np.exp(self.get_log_P(obs))
    
    def get_log_P(self, obs):
            T = obs.shape[0]-1
            log_alphas = self.get_Alphas(obs)
            return logsumexp(log_alphas[T,:])
            #return alphas[T,:].sum()
    
        
    def forward_backward(self, mfcc):
        alphas = self.get_Alphas(mfcc)
        betas = self.get_Betas(mfcc)
        #print("Alphas and betas:")
        #print(alphas)
        #print("______________________________________")
        #print(betas)
        gamma = np.zeros((mfcc.shape[0], self.N))
        for t in range(mfcc.shape[0]):
            for i in range(self.N):
                log_gamma = alphas[t,i]+betas[t,i]-self.get_log_P(mfcc)
                print("GAMMA FROM:")
                print(alphas.shape)
                print(betas[t,i])
                print(self.get_log_P(mfcc))
                #gamma[t,i]=alphas[t,i]*betas[t,i]/self.get_P(mfcc)
                gamma[t,i] = np.exp(log_gamma)
        
        xi = np.zeros((mfcc.shape[0]-1, self.N, self.N))
        for t in range(mfcc.shape[0]-1):
            for i in range(self.N):
                for j in range(self.N):
                    log_xi=alphas[t,i]+betas[t+1,j]+np.log(self.A[i,j])+self.get_log_B(mfcc[t+1], j)-self.get_log_P(mfcc)
                    #xi[t,i,j]=alphas[t,i]*betas[t+1,j]*self.A[i,j]*self.get_B(mfcc[t+1])/self.get_P(mfcc)
                    xi[t,i,j] = np.exp(log_xi)
        return gamma, xi

    def log_forward_backward(self, mfcc):
        alphas = self.get_Alphas(mfcc)
        betas = self.get_Betas(mfcc)
        gamma = np.zeros((mfcc.shape[0], self.N))
        for t in range(mfcc.shape[0]):
            for i in range(self.N):
                 gamma[t,i] = alphas[t,i]+betas[t,i]-self.get_log_P(mfcc)
        
        xi = np.zeros((mfcc.shape[0]-1, self.N, self.N))
        for t in range(mfcc.shape[0]-1):
            for i in range(self.N):
                for j in range(self.N):
                    xi[t,i,j]=alphas[t,i]+betas[t+1,j]+self.A[i,j]+self.get_log_B(mfcc[t+1], j)-self.get_log_P(mfcc)
        return gamma, xi

    
    
    def fit(self, train_mfccs, epochs=10):
        for i in range(epochs):
            for sample in train_mfccs:
                print("Sample is good")
                startA = self.A
                startM = self.m
                startS = self.S
                gammas, xis = self.forward_backward(sample)
              #  print("GAMMAS:")
               # print(gammas)
                #print("XIS:")
               # print("_________________________________________")
               # print(xis)
                for i in range(self.N):
                    self.PI[i] = gammas[0,i]/gammas[0,:].sum()
                sum1 = np.zeros((self.N, self.N))
                sum2 = np.zeros((self.N, self.N))
                for i in range(self.N):
                    for j in range(self.N):
                            for t in range(sample.shape[0]-1):
                                sum1[i, j] += xis[t,i,j]
                                sum2[i, j] += gammas[t,i]
                self.A = sum1/sum2
                
                for i in range(self.N):
                    sum1 = np.zeros(39)
                    sum2 = 0
                    for t in range(sample.shape[0]):
                        sum1 += gammas[t,i]*sample[t,:]
                        sum2 += gammas[t, i]
                    self.m[i, :] = sum1/sum2
                    sum1 = np.zeros(39)
                    for t in range(sample.shape[0]):
                        sum1 += gammas[t,i]*(sample[t,:]-self.m[i,:])**2
                    self.S[i, :] = sum1/sum2
               # print(self.A-startA)
                #print(self.m - startM)
                #print(self.S - startS)
                        
        # Run forward-backward to train hmm
        # TODO
    
    def log_fit(self, train_mfccs, epochs=10):
        for i in range(epochs):
            for sample in train_mfccs:
                print("GOOD SAMPLE!!!")
                startA = self.A
                startM = self.m
                startS = self.S
                log_gammas, log_xis = self.log_forward_backward(sample)
              #  print("GAMMAS:")
               # print(gammas)
                #print("XIS:")
               # print("_________________________________________")
               # print(xis)
                self.PI = log_gammas[0,:]
                #for i in range(self.N):
                 #   self.PI[i] = np.exp(log_gammas[0,i])#/gammas[0,:].sum()
                #sum1 = np.zeros((self.N, self.N))
                #sum2 = np.zeros((self.N, self.N))
                for i in range(self.N):
                    for j in range(self.N):
                            #self.A[i,j]=np.exp(logsumexp(log_xis[:,i,j])-logsumexp(log_gammas[:,i]))
                            self.A[i,j]=logsumexp(log_xis[:,i,j])-logsumexp(log_gammas[:,i])
                            #for t in range(sample.shape[0]-1):
                             #   sum1[i, j] += xis[t,i,j]
                              #  sum2[i, j] += gammas[t,i]
                #self.A = sum1/sum2
              #  print(np.array(sample).shape)
            
                #log_sample = np.log(np.array(sample))
              #  for i in range(self.N):
                     #for t in range(sample.shape[0]):
                      #      log_gammas[t,i]+sam
                #    sum1 = np.zeros(39)
                #    sum2 = 0
                #    for t in range(sample.shape[0]):
                #        sum1 += gammas[t,i]*sample[t,:]
                #        sum2 += gammas[t, i]
                for i in range(self.N):
                    log_gammas_sum = logsumexp(log_gammas[:,i])
                    gammas_normalized = np.array(list(map(lambda x:np.exp(x-log_gammas_sum), log_gammas[:,i])))
                    #print("_________________")
                    #print(sample)
                    #print("_________________")
                    self.m[i,:]=(gammas_normalized*sample.T).sum(axis = 1)
                    #print(self.m.shape)
                    self.S[i,:]=(gammas_normalized*((sample-self.m[i,:])**2).T).sum(axis = 1)
                    print(self.S[i,:])
                    #print(np.log(-1))
                  #  self.m[i, :] = np.exp(logsumexp(log_gammas[:i] + np.log(sample))-logsumexp(log_gammas[:,i]))
                #for i in range(self.N):
                    #print(self.m[i:].shape)
                #    self.S[i, :] = np.exp(logsumexp(log_gammas[:i] + np.log((sample-self.m[i,:])**2))-logsumexp(log_gammas[:,i]))
                #    sum1 = np.zeros(39)
                #    for t in range(sample.shape[0]):
                #        sum1 += gammas[t,i]*(sample[t,:]-self.m[i,:])**2
                #    self.S[i, :] = sum1/sum2
               # print(self.A-startA)
                #print(self.m - startM)
                #print(self.S - startS)
                        
        # Run forward-backward to train hmm
        # TODO
    
    
    def get_log_likelihood(self, mfcc):
        # TODO
        return self.get_P(mfcc)
        #return log_likelihood