from hmmlearn.hmm import GaussianHMM
import numpy as np
def myFunc(args):
    trainset, testset, states, iters = args
    lengths = list(map(lambda x: x.shape[0], trainset))
    model = GaussianHMM(n_components=states, covariance_type="diag", n_iter=iters, algorithm="viterbi", tol = 1e-10)
    train = np.concatenate(trainset)
    model.fit(train, lengths)
    scoring = []
    for sample in testset:
    	scoring.append(model.score(sample)) 
    return scoring
