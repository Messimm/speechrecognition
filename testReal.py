import math
from scipy.special import logsumexp

class HMM(object):

    def __init__(self, m0, S0, states = 10):
        
        np.random.seed(777)
        self.a = np.random.sample((states, states))
        for i in range(states):
            self.a[i,:]=self.a[i,:]/self.a[i,:].sum()
        self.a = np.log(self.a)
        self.N = states
        self.m = np.zeros((self.N+1, 39))
        self.S = np.zeros((self.N+1, 39))
        for i in range(1, self.N+1):
            self.m[i,:]=m0
            self.S[i,:]=S0
        self.m[0,:]=np.zeros(39)
        self.S[0,:]=np.zeros(39)
        
        self.pi = [1/self.N]*self.N
        #self.a = probabilities[1]
        #self.b = probabilities[2]
        
    def get_log_B(self, o, i):
        accum = 0
        for d in range(39):
            accum+=(np.log(2*math.pi)+self.S[i,d]+(o[d]-self.m[i,d])**2/self.S[i,d])
        return -0.5*accum

    @staticmethod
    def log_sum_exp(seq):
        if abs(min(seq)) > abs(max(seq)):
            a = min(seq)
        else:
            a = max(seq)

        total = 0
        for x in seq:
            total += math.exp(x - a)
        return a + math.log(total)

    def forward(self, sequence):
        N = self.N#len(self.pi)
        alpha = []

        d1 = {}
        for i in range(1, N+1):
            d1[i] = self.pi[i-1] + self.get_log_B(sequence[0],i)# b[i][sequence[0]]
        alpha.append(d1)

        for t in range(1, len(sequence)):
            d = {}
            o = sequence[t]

            for j in range(1, N+1):
                sum_seq = []
                for i in range(1, N+1):
                    sum_seq.append(alpha[-1][i] + self.a[i-1][j-1])

                d[j] = logsumexp(np.array(sum_seq)) + self.get_log_B(o, j)# b[j][o]

            alpha.append(d)

        return alpha

    def forward_probability(self, alpha):
        c = []
        for i in alpha[-1].values():
            c.append(i)
        return logsumexp(np.array(c))

    def backward(self, sequence):
        N = self.N#len(self.pi)
        T = len(sequence)
        beta = []

        dT = {}
        for i in range(1, N+1):
            dT[i] = 0
        beta.append(dT)

        for t in range(T - 2, -1, -1):
            d = {}
            o = sequence[t + 1]

            for i in range(1, N+1):
                sum_seq = []
                for j in range(1, N+1):
                    sum_seq.append(self.a[i-1][j-1] + self.get_log_B(o, j) + beta[-1][j]) #b[j][o]
                d[i] = logsumexp(np.array(sum_seq))

            beta.append(d)
        beta.reverse()
        return beta

    def backward_probability(self, beta, sequence):
        N = self.N#len(self.pi)
        sum_seq = []

        for i in range(1, N+1):
            sum_seq.append(self.pi[i-1] + self.get_log_B(sequence[0], i) + beta[0][i]) # b[i][sequence[0]]
        return logsumexp(np.array(sum_seq))

    def forward_backward(self, sequence):
        N = self.N#len(self.pi)
        alpha = self.forward(sequence)
        beta = self.backward(sequence)
        T = len(sequence)

        xis = []
        for t in range(T - 1):
            xis.append(self.xi_matrix(t, sequence, alpha, beta))
        gammas = []
        for t in range(T):
            gammas.append(self.gamma(t, sequence, alpha, beta, xis))

        pi_hat = gammas[0]
        a_hat = {}
        b_hat = {}
        
        m_hat = np.zeros((self.N+1, 39))
        S_hat = np.zeros((self.N+1, 39))
        for i in range(1, N+1):
            a_hat[i] = {}
            #b_hat[i] = {}
            sum_seq = []
            for t in range(T - 1):
                sum_seq.append(gammas[t][i])
            a_hat_denom = logsumexp(np.array(sum_seq))
            #my_sum_seq = list(map(lambda x: np.exp(x-a_hat_denom), sum_seq))
            for j in range(1, N+1):
                sum_seq = []
                for t in range(T - 1):
                    sum_seq.append(xis[t][i][j])
                a_hat_num = logsumexp(np.array(sum_seq))
                a_hat[i][j] = a_hat_num - a_hat_denom
            
            sum_seq = []
            for t in range(T):
                sum_seq.append(np.exp(gammas[t][i]))
            denom = np.maximum(np.array(sum_seq).sum(), 1e-8)
            #print(my_sum_seq)
            #print("________________________")
            
            #print(np.array(my_sum_seq).sum())
            #print("***************************")
            for t in range(T):
                   m_hat[i,:]+=sum_seq[t]*np.array(sequence[t])/denom
            #print("***************************")
            #print(m_hat[i,:])
            #print(i)
            #print("__________________________________")
            for t in range(T):
                S_hat[i,:]+=sum_seq[t]*((np.array(sequence[t])-m_hat[i,:])**2)/denom
            #print(S_hat[i,:])
            #print("////////////////////////////////////////")
            #    sum_seq.append(gammas[t][i])
            #b_hat_denom = self.log_sum_exp(sum_seq)
            #for k in self.b[i]:
            #    sum_seq = []
            #    for t in xrange(T):
            #        o = sequence[t]
            #        if o == k:
            #            sum_seq.append(gammas[t][i])
            #    b_hat_num = self.log_sum_exp(sum_seq)
            #    b_hat[i][k] = b_hat_num - b_hat_denom
        #print(m_hat)
        return (pi_hat, a_hat, m_hat, S_hat)

    def gamma(self, t, sequence, alpha, beta, xis):
        N = self.N
        gamma = {}
        if t < len(sequence) - 1:
            xi = xis[t]
            for i in range(1, N+1):
                sum_seq = []
                for j in range(1, N+1):
                    sum_seq.append(xi[i][j])
                gamma[i] = logsumexp(np.array(sum_seq))
        else:
            sum_seq = []
            for i in range(1, N+1):
                gamma[i] = alpha[t][i] + beta[t][i]
                sum_seq.append(gamma[i])

            denom = logsumexp(np.array(sum_seq))

            for i in range(1, N+1):
                gamma[i] -= denom

        return gamma

    def xi_matrix(self, t, sequence, alpha, beta):
        N = len(self.pi)
        o = sequence[t+1]

        xi = {}

        sum_seq = []

        for i in range(1, N+1):
            xi[i] = {}
            for j in range(1, N+1):
                num = alpha[t][i] + self.a[i-1][j-1] \
                      + self.get_log_B(o, j) + beta[t + 1][j] #b[j][o]
                sum_seq.append(num)
                xi[i][j] = num

        denom = logsumexp(np.array(sum_seq))

        for i in range(1, N+1):
            for j in range(1, N+1):
                xi[i][j] -= denom

        return xi

    def update(self, sequence, cutoff_value, n_iter):
        increase = cutoff_value + 1
        iterator = 0
        while (iterator<n_iter):#increase > cutoff_value#):
            iterator = iterator+1
            before = self.forward_probability(self.forward(sequence))
            new_p = self.forward_backward(sequence)
            new_new_p = []
            new_new_a = np.zeros((self.N, self.N))
            for i in range(1,self.N+1):
                new_new_p.append(new_p[0][i])
                for j in range(1, self.N+1):
                    new_new_a[i-1,j-1]=new_p[1][i][j]
                    
            #print(new_new_p)
            #print(type(new_p[0]))
            #print(np.array(list(new_p[0])))
            #print(np.array(new_new_p).shape)
            #print(new_p[2])
            self.pi = np.array(new_new_p)
            #print(np.array(new_p[1]))
            self.a = new_new_a#new_p[1]
            self.m = new_p[2]
            #print(self.m)
            self.S = new_p[3]
            after = self.forward_probability(self.forward(sequence))
            increase = after - before
            print(increase)
            increase = cutoff_value-1